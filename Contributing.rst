Contributing
============

If you’d like to contribute to Plasma Mobile, join our community! In
this `page <https://www.plasma-mobile.org/contributing/>`__ you will
find a brief introduction to things everyone in KDE should know, and
help you get started with contributing.
